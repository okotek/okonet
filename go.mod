module okonet

go 1.22.0

replace gitlab.com/okotek/okoframe => ../okoframe

require gitlab.com/okotek/okoframe v0.0.0-00010101000000-000000000000

require gocv.io/x/gocv v0.35.0 // indirect
