package okonet

import (
	"encoding/gob"
	"net"

	"gitlab.com/okotek/okoerr"

	"gitlab.com/okotek/okoframe"
)

func Listen(output chan okoframe.Frame) {

	var tmpFrame okoframe.Frame
	ln, lnErr := net.Listen("tcp", ":8645")
	if lnErr != nil {
		okoerr.ReportError("error getting ln on Listen", "net", lnErr)
	}

	for {
		con, conErr := ln.Accept()
		if conErr != nil {
			okoerr.ReportError("Failed to accept in okonet lnAccept:", "net", conErr)
		}

		go func() {
			decErr := gob.NewDecoder(con).Decode(&tmpFrame)
			if decErr != nil {
				okoerr.ReportError("Failed to decode into tmpFrame in okonet listen function", "net", decErr)
			}

			output <- tmpFrame
		}()
	}
}

func Sendoff(targ string, input chan okoframe.Frame) {

	con, conErr := net.Dial("tcp", targ)
	if conErr != nil {
		okoerr.ReportError("couldn't get connection in okonet sendoff", "net", conErr)
	}

	for tmpFrame := range input {

		go func(tf okoframe.Frame) {
			if encErr := gob.NewEncoder(con).Encode(&tf); encErr != nil {
				okoerr.ReportError("bad encode in okonet sendoff", "net", encErr)
			}

		}(tmpFrame)

	}
}
